<?php
/**
 * If .modman is not a symlink, try to link it to src. This is used as post-install composer script and on the server
 * as well if modman is used there.
 */
$modmanDir = realpath(__DIR__ . '/..') . DIRECTORY_SEPARATOR . '.modman';
$srcDir = realpath(__DIR__ . '/../src');
if (!is_link($modmanDir)) {
    echo "src => .modman symlink not found, trying to create...";
    if (!symlink($srcDir, $modmanDir)) {
        throw new RuntimeException("Symlink failed. Please make sure, there is no existing directory at {$modmanDir} and you have permissions to set symlinks.");
    }
    echo "OK\n";
}