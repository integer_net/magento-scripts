#!/usr/bin/env php
<?php
/**
 * Links shared resources like /media and /var from outside
 *
 * @deprecated
 */
?>
Please use "composer run-script shared-links" instead.