<?php
/**
 * integer_net Magento Scripts
 *
 * @category   IntegerNet
 * @package    IntegerNet\MagentoScripts
 * @copyright  Copyright (c) 2015 integer_net GmbH (http://www.integer-net.de/)
 * @author     Fabian Schmengler <fs@integer-net.de>
 */
namespace IntegerNet\MagentoScripts\Composer;
use Symfony\Component\Process\Process;

/**
 * Install frontend boilerplate package
 *
 * @package IntegerNet\MagentoScripts
 */
class InstallBoilerplate extends AbstractScript
{
    const FRONTEND_BOILERPLATE_PACKAGE = 'integernet/boilerplate';
    
    public function run()
    {
        $sourceRoot = $this->dirs->vendor() . DS . self::FRONTEND_BOILERPLATE_PACKAGE . DS . 'src';
        $targetRoot = $this->dirs->root();
        
        if (!is_dir($sourceRoot)) {
            $this->io->writeError(sprintf(
                    'Frontend boilerplate package is not installed. Install it with %s or remove %s from composer.json.',
                    'composer require ' . self::FRONTEND_BOILERPLATE_PACKAGE, __METHOD__));
            return;
        }
        
        $this->io->write('Copy files for Grunt and Bower...');
        // copy files to root
        foreach (['.bowerrc', 'bower.json', 'package.json', 'Gruntfile.js'] as $file) {
            $this->filesystem->copy($sourceRoot . DS . $file, $targetRoot . DS . $file, true);
        }
        // copy grunt directory
        $this->copyDirectory($sourceRoot . DS . 'grunt', $targetRoot . DS . 'grunt');
        $this->copyDirectory($sourceRoot . DS . 'mage', $targetRoot . DS . 'grunt' . DS . 'mage');

        // initialize gruntconfig
        if (! \file_exists($targetRoot . DS . 'gruntconfig.yaml')) {
            $this->io->write('Copy initial gruntconfig.yaml...');
            $this->filesystem->copy($sourceRoot . DS . 'gruntconfig.yaml', $targetRoot . DS . 'gruntconfig.yaml');
        }
        
        $this->io->write('Running npm install...');
        if (! $this->exec('npm install')) {
            return;
        }
        $this->io->write('Running bower install...');
        $this->exec('bower install --allow-root');
    }

    /**
     * Helper method to execute shell commands and capture STDERR and STDOUT
     * @param $command
     * @return bool
     */
    private function exec($command)
    {
        $process = new Process($command);
        $process->run(function($type, $output) {
            if ($type === Process::ERR) {
                $this->io->writeError($output);
            } else {
                $this->io->write($output);
            }
        });
        if (! $process->isSuccessful()) {
            $this->io->writeError(sprintf('%s failed with exit code %d', $command, $process->getExitCode()));
        }
        return $process->isSuccessful();
    }
}