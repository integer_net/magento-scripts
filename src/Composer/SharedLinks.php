<?php
/**
 * integer_net Magento Scripts
 *
 * @category   IntegerNet
 * @package    IntegerNet\MagentoScripts
 * @copyright  Copyright (c) 2015 integer_net GmbH (http://www.integer-net.de/)
 * @author     Fabian Schmengler <fs@integer-net.de>
 */
namespace IntegerNet\MagentoScripts\Composer;

/**
 * Links shared resources like /media and /var from outside
 *
 * @package IntegerNet\MagentoScripts
 */
class SharedLinks extends AbstractScript
{
    /**
     * Run the script
     *
     * @return void
     */
    public function run()
    {
        $sharedDir = $this->getSharedDirPath();
        $this->io->write("Using shared dir {$sharedDir}");
        $wwwDir = $this->dirs->www();
        $this->copyTemplateLocalXml($sharedDir, $wwwDir);
        $this->createMissingSymlinks($wwwDir, $sharedDir);
    }

    /**
     * If SHARED_DIR environment variable is not set,  try to find the shared dir in the following
     * locations relative to project repository:
     *
     *  - ./shared          (inside repository)
     *  - ../shared         (next to repository)
     *  - ../../shared      (in grandparent of repository, this is the default unibox setup)
     *
     * @return string
     */
    private function getSharedDirPath()
    {
        $sharedDirCandidates = array(
            $this->dirs->root() . '/shared',
            $this->dirs->root() . '/../shared',
            $this->dirs->root() . '/../../shared',
        );
        if (getenv('SHARED_DIR')) {
            array_unshift($sharedDirCandidates, getenv('SHARED_DIR'));
        }
        foreach ($sharedDirCandidates as $sharedDir) {
            $sharedDirPath = realpath($sharedDir);
            if ($sharedDirPath) {
                break;
            }
        }
        while (empty($sharedDirPath)) {
            $createSharedDir = trim($this->io->askConfirmation("<question>Path to shared dir '$sharedDir' not found. Create?</question>"));
            if ($createSharedDir) {
                $this->filesystem->mkdir($sharedDir);
            } else {
                $sharedDir = trim($this->io->ask("<question>Please enter path to shared dir:</question>"));
            }
            $sharedDirPath = realpath($sharedDir);
        }
        return $sharedDirPath;
    }

    private function copyTemplateLocalXml($sharedDir, $wwwDir)
    {
        if (!is_readable($sharedDir . '/local.xml')
            && !is_readable($wwwDir . '/app/etc/local.xml')
            && !is_link($wwwDir . '/app/etc/local.xml')
        ) {
            $this->filesystem->copy($this->dirs->root() . '/etc/local.xml', $sharedDir . '/local.xml');
        }
    }

    /**
     * @param $wwwDir
     * @param $sharedDir
     */
    private function createMissingSymlinks($wwwDir, $sharedDir)
    {
        foreach (['media' => 'media', 'var' => 'var', '.htaccess' => '.htaccess', 'app/etc/local.xml' => 'local.xml'] as $link => $target) {
            if (!is_link($wwwDir . DIRECTORY_SEPARATOR . $link)) {
                if (!is_readable($sharedDir . DIRECTORY_SEPARATOR . $target)
                    && is_readable($wwwDir . DIRECTORY_SEPARATOR . $link)
                    && !is_link($wwwDir . DIRECTORY_SEPARATOR . $link)
                ) {
                    $this->io->write([
                        "<warning>Target {$sharedDir}/{$target} not found, but existing file or directory at {$wwwDir}/{$link}</warning>",
                        "<warning>Moving to shared dir...</warning>"
                    ]);
                    try {
                        $this->filesystem->rename($wwwDir . DIRECTORY_SEPARATOR . $link, $sharedDir . DIRECTORY_SEPARATOR . $target, true);
                        $this->io->write('<info>OK, moved.</info>');
                    } catch (\Exception $e) {
                        $this->io->writeError('<error>Rename failed.</error>');
                    }
                } elseif (!is_readable($sharedDir . DIRECTORY_SEPARATOR . $target)
                    && !is_readable($wwwDir . DIRECTORY_SEPARATOR . $link)
                ) {
                    $this->io->writeError("<warning>Neither {$sharedDir}/{$target} nor {$wwwDir}/{$link} found. Create either and re-run script.</warning>");
                    continue;
                } elseif (is_readable($wwwDir . DIRECTORY_SEPARATOR . $link) && !is_link($wwwDir . DIRECTORY_SEPARATOR . $link)) {
                    $backupDir = $sharedDir . DIRECTORY_SEPARATOR . "backup" . time();
                    $this->io->write([
                        "<warning>Target {$sharedDir}/{$target} exists, but is not a symlink</warning>",
                        "<warning>Moving to $backupDir...</warning>"
                    ]);
                    $this->filesystem->mkdir($backupDir);
                    $this->filesystem->rename($wwwDir . DIRECTORY_SEPARATOR . $link, $backupDir . DIRECTORY_SEPARATOR . $link, false);
                }
                $this->io->write("Trying to create {$link} symlink...");
                try {
                    $this->filesystem->symlink($sharedDir . DIRECTORY_SEPARATOR . $target, $wwwDir . DIRECTORY_SEPARATOR . $link);
                } catch (\Exception $e) {
                    $this->io->writeError("<error>Symlink failed. Please make sure, there is no existing file or directory at {$wwwDir}/{$link} and you have permissions to set symlinks.</error>");
                    return;
                }
                $this->io->write("<info>OK, symlink created.</info>");
            } else {
                $this->io->write("<info>Link {$wwwDir}/{$link} already exists. Skipped.</info>");
            }
        }
    }
}
