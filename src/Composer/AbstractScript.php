<?php
/**
 * integer_net Magento Scripts
 *
 * @category   IntegerNet
 * @package    IntegerNet\MagentoScripts
 * @copyright  Copyright (c) 2015 integer_net GmbH (http://www.integer-net.de/)
 * @author     Fabian Schmengler <fs@integer-net.de>
 */
namespace IntegerNet\MagentoScripts\Composer;

use Symfony\Component\Filesystem\Filesystem;
use Composer\IO\IOInterface;
use IntegerNet\MagentoScripts\ComposerDirectories;
use IntegerNet\MagentoScripts\ComposerConfig;

/**
 * @var string Namespaced constant as shortcut
 */
const DS = DIRECTORY_SEPARATOR;

/**
 * Base class for composer scripts
 */
abstract class AbstractScript
{
    /**
     * @var ComposerDirectories
     */
    protected $dirs;
    /**
     * @var ComposerConfig
     */
    protected $conf;
    /**
     * @var IOInterface
     */
    protected $io;
    /**
     * @var Filesystem
     */
    protected $filesystem;

    public function __construct(ComposerDirectories $directories, ComposerConfig $config, IOInterface $io)
    {
        $this->filesystem = new Filesystem();
        $this->dirs = $directories;
        $this->conf = $config;
        $this->io = $io;
    }

    /**
     * Helper method to copy directories recursively (like `cp -r`)
     *
     * Unfortunately neither plain PHP nor Symfony\Filesystem supports this
     *
     * @param string $sourceDir Source directory
     * @param string $targetDir Target directory (will contain everything that is contained in source directory)
     */
    protected function copyDirectory($sourceDir, $targetDir)
    {
        $iterator = new \RecursiveIteratorIterator(
            new \RecursiveDirectoryIterator($sourceDir, \FilesystemIterator::SKIP_DOTS),
            \RecursiveIteratorIterator::LEAVES_ONLY);
        foreach ($iterator as $path => $file) {
            /** @var \SplFileInfo $file */
            $relativePath = rtrim($this->filesystem->makePathRelative($path, $sourceDir), '/');
            $this->filesystem->copy($path, $targetDir . DS . $relativePath, true);
        }
    }
    /**
     * Run the script
     * 
     * @return void
     */
    abstract public function run();
}