<?php
/**
 * integer_net Magento Scripts
 *
 * @category   IntegerNet
 * @package    IntegerNet\MagentoScripts
 * @copyright  Copyright (c) 2015 integer_net GmbH (http://www.integer-net.de/)
 * @author     Fabian Schmengler <fs@integer-net.de>
 */
namespace IntegerNet\MagentoScripts\Composer;

/**
 * Install script to be triggered by "create project" event or manually
 *
 * @package IntegerNet\MagentoScripts\Composer
 */
class Install extends AbstractScript
{
    /**
     * Run the script
     *
     * @return void
     */
    public function run()
    {
        $this->init();
        $this->initDev();
    }

    /**
     * @todo make this script independent from composer, if it should be used to initialize staging and production server as well.
     */
    private function init()
    {
        if ($this->io->askConfirmation('Create "shared" directory outside of repository (Y/N)? ')) {
            $sharedDir = realpath($this->dirs->root() . DS . '..' . DS . 'shared');
            $sharedDir = $this->io->ask('Location:', $sharedDir);
            $this->io->write('Creating directories...', false);
            $this->filesystem->mkdir($sharedDir);
            $this->filesystem->mkdir($sharedDir . DS . 'media');
            $this->filesystem->mkdir($sharedDir . DS . 'log');
            $this->filesystem->mkdir($sharedDir . DS . 'session');
            $this->io->write('OK');
            $this->io->write('Creating symlinks...', false);
            $this->filesystem->symlink($sharedDir . DS . 'media', $this->dirs->www() . DS . 'media');
            $this->filesystem->symlink($sharedDir . DS . 'log', $this->dirs->www() . DS . 'var' . DS . 'log');
            $this->filesystem->symlink($sharedDir . DS . 'session', $this->dirs->www() . DS . 'var' . DS . 'session');
            //TODO generate local.xml (using magerun?)
            //TODO create symlink for local.xml
            //TODO copy existing content of media
        }
    }
    private function initDev()
    {
        if ($this->io->askConfirmation('Install frontend boilerplate? (Y/N) ')) {
            $script = new InstallBoilerplate($this->dirs, $this->conf, $this->io);
            $script->run();
        }
        //TODO optionally import a database or create a new one (n98-magerun)
    }
}