<?php
/**
 * integer_net Magento Scripts
 *
 * @category   IntegerNet
 * @package    IntegerNet\MagentoScripts
 * @copyright  Copyright (c) 2015 integer_net GmbH (http://www.integer-net.de/)
 * @author     Fabian Schmengler <fs@integer-net.de>
 */
namespace IntegerNet\MagentoScripts\Composer;

/**
 * Generate modman file from package.xml for Magento Connect modules installed via composer 
 *
 * @package IntegerNet\MagentoScripts
 */
class Package2Modman extends AbstractScript
{
    public function run()
    {
        $modmanDir = $this->dirs->src();
        foreach (new \FilesystemIterator($modmanDir) as $path => $file) {
            /** @var \SplFileInfo $file */
            if (! \file_exists($path . DS . 'modman') && \file_exists($path . DS . 'package.xml')) {
                $this->io->write(sprintf('%s does not contain a modman file, autogenerating from package.xml...',
                    $path));
                \file_put_contents($path . DS . 'modman', $this->convert($path . DS . 'package.xml'));
            }
        }
    }
    /**
     * Returns content for modman file of given package.xml
     * 
     * @param string $pathToPackageXml
     * @return string
     */
    public function convert($pathToPackageXml)
    {
        require_once $this->dirs->www() . DS . 'app' . DS . 'Mage.php';
        $package = new \Mage_Connect_Package($pathToPackageXml);
        $modmanDefinition = array();
        foreach ($package->getContents() as $path) {
            $path = str_replace('\\', '/', $path);
            $path = preg_replace('{^\./}', '', $path);
            $path = preg_replace('{^app/code/(.*?)/(.*?)/(.*?)/(.*)$}', 'app/code/$1/$2/$3', $path);
            $path = preg_replace('{^lib/(.*?)/(.*)$}', 'lib/$1', $path);
            $path = preg_replace('{^js/(.*?)/(.*?)/(.*)$}', 'js/$1', $path);
            $path = preg_replace('{^app/design/(.*?)/(.*?)/default/layout/(.*?)/(.*)$}', 'app/design/$1/$2/default/layout/$3', $path);
            $path = preg_replace('{^app/design/(.*?)/(.*?)/default/template/(.*?)/(.*)$}', 'app/design/$1/$2/default/template/$3', $path);
            $path = preg_replace('{^skin/(.*?)/(.*?)/default/(.*?)/(.*?)/(.*)$}', 'skin/$1/$2/default/$3/$4', $path);
            $modmanDefinition[$path] = $path;
        }
        $result = '';
        foreach ($modmanDefinition as $source => $target) {
            $result .= sprintf("%s %s\n", $source, $target);
        }
        return $result;
    }
}