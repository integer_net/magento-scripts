<?php
/**
 * integer_net Magento Scripts
 *
 * @category   IntegerNet
 * @package    IntegerNet\MagentoScripts
 * @copyright  Copyright (c) 2017 integer_net GmbH (http://www.integer-net.de/)
 * @author     Fabian Schmengler <fs@integer-net.de>
 */
namespace IntegerNet\MagentoScripts\Composer;

/**
 * Setup project on a new machine
 *
 * @package IntegerNet\MagentoScripts
 */
class SetupProject extends AbstractScript
{
    /**
     * Run the script
     *
     * @return void
     */
    public function run()
    {
        $wwwDir = $this->dirs->www();
        $sharedDir = $this->getSharedDirPath();
        $this->io->write("Using shared dir {$sharedDir}");
        $this->symlinkModmanDir();
        $this->copyTemplateLocalXml($sharedDir, $wwwDir);
    }

    /**
     * If SHARED_DIR environment variable is not set,  try to find the shared dir in the following
     * locations relative to project repository:
     *
     *  - ./shared          (inside repository)
     *  - ../shared         (next to repository)
     *  - ../../shared      (in grandparent of repository, this is the default unibox setup)
     *
     * @return string
     */
    private function getSharedDirPath()
    {
        $sharedDirCandidates = array(
            $this->dirs->root() . '/shared',
            $this->dirs->root() . '/../shared',
            $this->dirs->root() . '/../../shared',
        );
        if (getenv('SHARED_DIR')) {
            array_unshift($sharedDirCandidates, getenv('SHARED_DIR'));
        }
        foreach ($sharedDirCandidates as $sharedDir) {
            $sharedDirPath = realpath($sharedDir);
            if ($sharedDirPath) {
                break;
            }
        }
        while (empty($sharedDirPath)) {
            $createSharedDir = trim($this->io->askConfirmation("<question>Path to shared dir '$sharedDir' not found. Create?</question>"));
            if ($createSharedDir) {
                $this->filesystem->mkdir($sharedDir);
            } else {
                $sharedDir = trim($this->io->ask("<question>Please enter path to shared dir:</question>"));
            }
            $sharedDirPath = realpath($sharedDir);
        }
        return $sharedDirPath;
    }

    private function copyTemplateLocalXml($sharedDir, $wwwDir)
    {
        if (!is_readable($sharedDir . '/local.xml')
            && !is_readable($wwwDir . '/app/etc/local.xml')
            && !is_link($wwwDir . '/app/etc/local.xml')
        ) {
            $this->filesystem->copy($this->dirs->root() . '/etc/local.xml', $sharedDir . '/local.xml');
        }
    }

    private function symlinkModmanDir()
    {
        $modmanDir = $this->dirs->root() . '.modman';
        if (!is_readable($modmanDir) && !is_link($modmanDir)) {
            $this->filesystem->symlink($this->dirs->src(), $modmanDir);
        }
    }
}