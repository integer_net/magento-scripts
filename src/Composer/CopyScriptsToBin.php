<?php
/**
 * integer_net Magento Scripts
 *
 * @category   IntegerNet
 * @package    IntegerNet\MagentoScripts
 * @copyright  Copyright (c) 2015 integer_net GmbH (http://www.integer-net.de/)
 * @author     Fabian Schmengler <fs@integer-net.de>
 */
namespace IntegerNet\MagentoScripts\Composer;

/**
 * Copy all files (non-recursively) in bin to the /bin directory in the project repository where they will be versioned and
 * packaged in deployments.
 *
 * @package IntegerNet\MagentoScripts
 */
class CopyScriptsToBin extends AbstractScript
{
    const SCRIPTS_PACKAGE = 'integernet/magento-scripts';
    
    public function run()
    {
        $sourceDir = $this->dirs->vendor() . DS. self::SCRIPTS_PACKAGE . DS . 'bin';
        $targetDir = $this->dirs->bin();

        $copied = 0;
        foreach (new \FilesystemIterator($sourceDir) as $sourcePath => $sourceFile) {
            /** @var \SplFileInfo $file */
            try {
                $targetPath = $targetDir . DS . $sourceFile->getBasename();
                $this->filesystem->copy($sourcePath, $targetPath, true);
                $this->filesystem->chmod($targetPath, 0775);
                $copied++;
            } catch (IOException $e) {
                $this->io->writeError($e->getMessage());
            }
        }
        $this->io->write(
            sprintf('%d script files from integernet/magento-scripts copied to /bin', $copied));
    }
}