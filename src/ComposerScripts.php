<?php
/**
 * integer_net Magento Scripts
 *
 * @category   IntegerNet
 * @package    IntegerNet\MagentoScripts
 * @copyright  Copyright (c) 2015 integer_net GmbH (http://www.integer-net.de/)
 * @author     Fabian Schmengler <fs@integer-net.de>
 */
namespace IntegerNet\MagentoScripts;

use Composer\Script\Event;
use IntegerNet\MagentoScripts\Composer\CopyScriptsToBin;
use IntegerNet\MagentoScripts\Composer\Install;
use IntegerNet\MagentoScripts\Composer\InstallBoilerplate;
use IntegerNet\MagentoScripts\Composer\Package2Modman;
use IntegerNet\MagentoScripts\Composer\SharedLinks;

/**
 * This class contains static methods that are used as composer scripts. Only add scripts for development here.
 *
 * @package IntegerNet\MagentoScripts
 */
final class ComposerScripts
{
    public static function install(Event $event)
    {
        $script = new Install(new ComposerDirectories($event), new ComposerConfig($event), $event->getIO());
        $script->run();
    }
    public static function installFrontendBoilerplate(Event $event)
    {
        $script = new InstallBoilerplate(new ComposerDirectories($event), new ComposerConfig($event), $event->getIO());
        $script->run();
    }
    public static function convertPackageXmlToModman(Event $event)
    {
        $script = new Package2Modman(new ComposerDirectories($event), new ComposerConfig($event), $event->getIO());
        $script->run();
    }
    public static function copyScriptsToBin(Event $event)
    {
        $script = new CopyScriptsToBin(new ComposerDirectories($event), new ComposerConfig($event), $event->getIO());
        $script->run();
    }
    public static function sharedLinks(Event $event)
    {
        $script = new SharedLinks(new ComposerDirectories($event), new ComposerConfig($event), $event->getIO());
        $script->run();
    }
}