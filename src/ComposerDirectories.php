<?php
/**
 * integer_net Magento Scripts
 *
 * @category   IntegerNet
 * @package    IntegerNet\MagentoScripts
 * @copyright  Copyright (c) 2015 integer_net GmbH (http://www.integer-net.de/)
 * @author     Fabian Schmengler <fs@integer-net.de>
 */
namespace IntegerNet\MagentoScripts;

use Composer\Script\Event;

/**
 * @var string Namespaced constant as shortcut
 */
const DS = DIRECTORY_SEPARATOR;

/**
 * Access project directories
 */
class ComposerDirectories
{
    /**
     * @var Event
     */
    private $event;
    
    public function __construct(Event $event)
    {
        $this->event = $event;
    }
    public function root()
    {
        return getcwd();
    }
    public function vendor()
    {
        return $this->event->getComposer()->getConfig()->get('vendor-dir') ;
    }
    public function src()
    {
        return $this->root() . DS . 'src';
    }
    public function bin()
    {
        return $this->root() . DS . 'bin';
    }
    public function www()
    {
        /*
         * Possible to retrieve from composer installer configuration with
         *  $this->event->getComposer()->getPackage()->getExtra();
         *  
         * But let's just trust our convention:
         */
        return $this->root() . DS . 'www';
    }
    
}