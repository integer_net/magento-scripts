<?php
/**
 * integer_net Magento Scripts
 *
 * @category   IntegerNet
 * @package    IntegerNet\MagentoScripts
 * @copyright  Copyright (c) 2015 integer_net GmbH (http://www.integer-net.de/)
 * @author     Fabian Schmengler <fs@integer-net.de>
 */
namespace IntegerNet\MagentoScripts;

use Composer\Script\Event;

/**
 * Access composer configuratin values
 */
class ComposerConfig
{
    private $event;
    
    public function __construct(Event $event)
    {
        $this->event = $event;
    }
    /**
     * Returns path to PHP executable. It can be configured in composer.json or COMPOSER_HOME/config.json:
     * 
     *   "extra": {
     *     "magento-scripts": {
     *       "php-bin": "/path/to/php"
     *     }
     *   }
     * @return mixed
     */
    public function phpBin()
    {
        return $this->getExtraConfig('php-bin', 'php');
    }
    /**
     * Returns configuration from project repository or COMPOSER_HOME/config.json
     *
     * It's looking for the key within extra > magento-scripts
     *
     * @param string $key
     * @param mixed $default
     * @return mixed
     */
    private function getExtraConfig($key, $default = null)
    {
        $extra = $this->event->getComposer()->getPackage()->getExtra();
        if (array_key_exists('magento-scripts', $extra) && array_key_exists($key, $extra['magento-scripts'])) {
            return $extra['magento-scripts'][$key];
        }
        return $default;
    }
    
}